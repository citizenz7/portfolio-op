# Portfolio d'Olivier Prieur

Portfolio d'Olivier Prieur réalisé avec le framework Symfony 5.

## Fonctionnalités :
* Docker
* Tests unitaires
* Pipeline Gitlab CI/CD
* Commande de création de compte admin

## Environnement de développement

### Pré-requis

* PHP 7.4+
* Composer
* Symfony CLI
* Docker
* Docker-compose

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (issue de la CLI Symfony) :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
composer install
docker-compose up -d
symfony serve -d
```

### Création du compte admin
```
symfony console app:create-user EMAIL PASSWORD
```
Ce qui donnera par exemple :
```
symfony console app:create-user john.doe@gmail.com password
```

### Tests unitaires
Jouer les tests :
```
php bin/phpunit --testdox
```

Créer d'autres tests :
```
symfony console make:unit-test
```